import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

/// Taps a widget that contains text.
///
/// Examples:
///
///   `Then I tap the label that contains the text "Logout"`
///   `Then I tap the button that contains the text "Sign up"`
///   `Then I tap the widget that contains the text "My User Profile"`
StepDefinitionGeneric tapWidgetWithTextStep() {
  return then1<String, FlutterWorld>(
    RegExp(r'I tap the (?:button|element|label|field|text|widget) that contains the text {string}$'),
    (input1, context) async {
      final finder = context.world.appDriver.findBy(input1, FindType.text);
      print("to tap: $finder");
      await context.world.appDriver.scrollIntoView(finder);
      await context.world.appDriver.tap(finder);
    },
  );
}

/// Select and scroll a dropdown list in a given direction
///
/// Examples:
///
///   `Then I scroll the dropdown list "LanguageList" by -10`
StepDefinitionGeneric scrollDropDown() {
  return then2<String, double, FlutterWorld>(
    RegExp(r'I scroll the dropdown list {string} by {num}'),
    (input1, distance, context) async {
      final finder = context.world.appDriver.findBy(input1, FindType.key);
      await context.world.appDriver.tap(finder);
      await context.world.appDriver.waitForAppToSettle();
      final dropdownListFinder = context.world.appDriver.findBy(Scrollable, FindType.type);
      await context.world.appDriver.scroll(dropdownListFinder.last, dy: distance);
      await context.world.appDriver.waitForAppToSettle();
    },
  );
}

/// Taps a dropdown that contains text.
///
/// Examples:
///
///   `Then I tap the dropdown button that contains the text "Logout"`
StepDefinitionGeneric selectDropDownWithTextStep() {
  return then1<String, FlutterWorld>(
    RegExp(r'I tap the dropdown button that contains the text {string}$'),
    (input1, context) async {
      final finder = context.world.appDriver.findBy(input1, FindType.text);
      print("to tap: $finder");
      await context.world.appDriver.scrollIntoView(finder.last);
      await context.world.appDriver.tap(finder.last);
    },
  );
}
